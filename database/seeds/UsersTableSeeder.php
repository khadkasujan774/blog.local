<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    const  TOTAL_DATA = 500;

    public function run()
    {

        $data = $this->makeData();


        DB::table('users')->insert($data);
    }

    private function makeData()
    {
        $faker = \Faker\Factory::create('np_NP');
        $data = [];
        $tmp = [
            'username' => 'admin',
            'email' => 'tt@tt.tt',
            'email_verified_at' => $faker->dateTime,
            'password' => bcrypt('pass'),
            'remember_token' => ''
        ];
        $data[] = $tmp;

        for ($i = 0; $i < static::TOTAL_DATA; $i++) {
            $tmp = [
                'username' => $faker->userName,
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime,
                'password' => bcrypt($faker->text),
                'remember_token' => ''
            ];
            $data[] = $tmp;
        }
        return $data;
    }
}
