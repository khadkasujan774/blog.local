<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->string('brand');
            $table->string('model_name');
            $table->integer('price');
            $table->text('display');
            $table->string('color');
            $table->text('os');
            $table->integer('ram');
            $table->integer('storage');
            $table->integer('rear_camera');
            $table->integer('front_camera');
            $table->integer('battery_capacity');
            $table->text('other_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobiles');
    }
}
