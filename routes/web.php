<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('news', 'NewsController');


//Route::delete('/news/{news}/{comment}', 'NewsController@destroy');

//GET NEWLIST WITH AJAX
Route::get('/newslist', function () {
    return \App\News::with('comments')->get();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//login
Route::get('/login', 'LoginController@index');
Route::post('/login', 'LoginController@login');
Route::post('/logout', 'LoginController@logout');
Route::get('/logout', 'LoginController@logout');


Route::group(['middleware' => ['Authcheck']], function () {
    Route::get('news', 'NewsController@index');
    Route::get('/news/{id}', 'NewsController@getSingle');
    Route::post('/news/{news}/comments', 'NewsController@addComment');
});

Route::get('/apps', 'AppsController@index');
Route::get('/apps/add', 'AppsController@add');
Route::post('/apps', 'AppsController@store');
Route::get('/apps/show', 'AppsController@show');
Route::delete('/apps/{app}', 'AppsController@destroy');
Route::post('/apps/{apps}', 'AppsController@download');
Route::get('extra', 'ExtraController@index');
Route::get('startup', 'StartupController@index');

/*Route::get('skills', function () {
    return['Laravel', 'Vue', 'PHP', 'JavaScript', 'Tooling'];
});*/

Route::get('mobiles', 'MobilesController@index');
Route::get('/mobiles/addmobile', 'MobilesController@add');
Route::post('/mobiles', 'MobilesController@store');
Route::get('/mobiles', 'MobilesController@show');
Route::get('/mobiles/{mobile}', 'MobilesController@getDetails');
Route::get('/welcome', 'UserController@getUsers');
Route::any('/search', 'UserController@searchUser');
