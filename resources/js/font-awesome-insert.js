import { library } from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

import {
    faCoffee,
    faAngleDoubleDown,
    faChessBoard,
    faDollarSign,
    faEnvelope,
    faBullhorn,
    faGlobeAsia,
    faUser,
    faMapMarkedAlt

} from '@fortawesome/free-solid-svg-icons'

import {

} from '@fortawesome/free-brands-svg-icons'

library.add({
    faCoffee,
    faAngleDoubleDown,
    faChessBoard,
    faDollarSign,
    faEnvelope,
    faBullhorn,
    faGlobeAsia,
    faUser,
    faMapMarkedAlt


});
Vue.component('fa', FontAwesomeIcon);
