<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <meta id="csrf" name="csrf-token" content="{{ csrf_token() }}">
    <title></title>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
<div id="style">
    @yield('content')
{{--<div class="test-modal">
    aaaaaaaaaaaaa
</div>--}}
{{--
<ul class="test-menu">
    <li><a class="active" href="/">Home</a></li>
    <li><a href="/news">News</a></li>
    <li><a href="apps">APPS</a></li>
    <li><a href="#contact">MOBILE</a></li>
    <li><a href="#contact">MEDIA</a></li>
   --}}
{{-- <li class="submenu">
        <a href="#contact">DEV & DESIGN</a>
        <ul>
            <li><a href="#">test1</a></li>
            <li class="submenu">
                <a href="#">test2</a>
                <ul>
                    <li><a href="#">test test 1</a></li>
                    <li><a href="#">test test 2</a></li>
                </ul>
            </li>
            <li><a href="#">test3</a></li>
        </ul>
    </li>--}}{{--

</ul>
--}}
</div>
<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
