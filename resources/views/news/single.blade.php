
@extends('layout')

@section('content')



<h2 class="title">{{ $news->title }}</h2>
    <p>{{ $news->description }}</p>


    <form method="post" action="/news/{{ $news->id }}/comments">

        {{ csrf_field() }}
        <div class="field">

            <div class="control">
                <textarea name="description" class="textarea" required></textarea>

            </div>
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-success">Add comments</button>
            </div>
        </div>


        @foreach($news->comments as $comment)
             <p>{{ $comment->comment }}</p>
        @endforeach
    </form>

@endsection
