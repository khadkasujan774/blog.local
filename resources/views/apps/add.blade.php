@extends('layout')

@section('content')

     <h1>Upload a File</h1>
     <form action="/apps" method="post" enctype="multipart/form-data">
         @csrf
         <input type="file" name="image[]" multiple><br>
         <input type="submit" value="Upload">
     </form>

    @endsection
