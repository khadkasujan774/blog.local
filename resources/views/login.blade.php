@extends('layout')

@section('content')
    <div class="container">
        <div>
            <login-form usernotfound="{{ session()->has('auth') }}"></login-form>
        </div>
    </div>
@endsection
