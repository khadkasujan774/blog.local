@extends('layout')

@section('content')

    <table class="table is-bordered">
        <thead>
        <tr>
            <th>id</th>
            <th>username</th>
            <th>Email</th>
            <th>email_verified_at</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->username}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->email_verified_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endsection
