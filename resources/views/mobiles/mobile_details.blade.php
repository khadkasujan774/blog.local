@extends('layout')
@section('content')

<ul  style="list-style-type:disc">
    <li>{{ $mobile->brand }}</li>
    <li>{{ $mobile->model_name }}</li>
    <li>{{ number_format($mobile->price) }}</li>
    <li>{{ $mobile->display }}</li>
    <li>{{ $mobile->color }}</li>
    <li>{{ $mobile->os }}</li>
    <li>{{ $mobile->ram }} GB</li>
    <li>{{ $mobile->storage }} GB</li>
    <li>{{ $mobile->rear_camera }} MP</li>
    <li>{{ $mobile->other_details }}</li>
    <li>{{ $mobile->front_camera }} MP</li>
    <li>{{ $mobile->battery_capacity }} mah</li>
</ul>



@endsection

