@extends('layout')

@section('content')
    <a class="button is-dark" style="margin:2em" href="/mobiles/addmobile">Add</a><br>
               <form action="#">
                <input type="text" placeholder="Search.." name="search">
                <button type="submit"><i class="fas fa-search"></i></button>
              </form>
  <section class="section">
      <div class="columns">
          <div class="column is-one-fifth">
              <p>Brand</p>
              <label class="checkbox">
                  <input type="checkbox">
                  Samsung
              </label><br>
              <label class="checkbox">
                  <input type="checkbox">
                  Apple
              </label><br>
              <label class="checkbox">
                  <input type="checkbox">
                  Colors
              </label><br>
              <label class="checkbox">
                  <input type="checkbox">
                  Nokia
              </label>
          </div>
          <div class="column">
              <div class="flex-container">
              {{--<p>Total page is {{$mm->total()}} </p>
                  <p>current page is {{ $mm->currentPage() }}</p>--}}
                  @foreach($mm as $mobile)
                      <div>
                          <a target="" href="/mobiles/{{$mobile -> id}}">
                              <img src="{{asset('storage/'.$mobile->image)}}" alt="image">
                          </a>
                          <h1 class="text">{{$mobile -> brand}}</h1>
                          <h1 class="text">{{$mobile -> model_name}}</h1>
                          <h1 class="price">Rs.{{number_format($mobile -> price)}}</h1>
                          {{--<h1 class="text">{{$mobile -> description}}</h1>--}}
                      </div>
                  @endforeach
              </div>
          </div>
      </div>
  </section>
    <div class="pagination">
        <p>{{ $mm->links() }}</p>
    </div>





@endsection
