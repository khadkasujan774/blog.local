@extends('layout')
@section('content')

    <form action="/mobiles" method="post" enctype="multipart/form-data">
        @csrf
        <div class="field " style="margin-left: 3em">
            <div class="control">
                <input type="file" name="image"><br>
                <span style="color: red">{{ $errors->first('image') }}</span>

                <label class="label">Brand:</label>
                <input type="text" class="input is-success" name="brand" value="{{old('brand')}}">
                <span style="color: red">{{ $errors->first('brand') }}</span>

                <label class="label">Model_name:</label>
                <input class="input is-success" type="text" name="model_name" value="{{old('model_name')}}">
                <span style="color: red">{{ $errors->first('model_name') }}</span>

                <label class="label">Price:</label>
                <input class="input is-success" type="text" name="price" value="{{old('price')}}">
                <span style="color: red">{{ $errors->first('price') }}</span>

                {{--<label class="label">SIM Type:</label>
                <input class="input is-success" type="text" name="sim_typt" value="{{old('sim_type')}}">
                <span style="color: red">{{ $errors->first('sim_type') }}</span>--}}

                <label class="label">Display:</label>
                <input class="input is-success" type="text" name="display" value="{{old('display')}}">
                <span style="color: red">{{ $errors->first('display') }}</span>

                <label class="label">Color:</label>
                <input class="input is-success" type="text" name="color" value="{{old('color')}}">
                <span style="color: red">{{ $errors->first('color') }}</span>

                <label class="label">OS:</label>
                <input class="input is-success" type="text" name="os" value="{{old('os')}}">
                <span style="color: red">{{ $errors->first('os') }}</span>

                <label class="label">RAM:</label>
                <input class="input is-success" type="text" name="ram" value="{{old('ram')}}">
                <span style="color: red">{{ $errors->first('ram') }}</span>

                <label class="label">Storage:</label>
                <input class="input is-success" type="text" name="storage" value="{{old('storage')}}">
                <span style="color: red">{{ $errors->first('storage') }}</span>

                <label class="label">Rear Camera:</label>
                <input class="input is-success" type="text" name="rear_camera" value="{{old('rear_camera')}}">
                <span style="color: red">{{ $errors->first('rear_camera') }}</span>

                <label class="label">Front Camera:</label>
                <input class="input is-success" type="text" name="front_camera" value="{{old('front_camera')}}">
                <span style="color: red">{{ $errors->first('front_camera') }}</span>

                <label class="label">Battery Capacity:</label>
                <input class="input is-success" type="text" name="battery_capacity" value="{{old('battery_capacity')}}">
                <span style="color: red">{{ $errors->first('battery_capacity') }}</span>

                <label class="label" for="other_details">Other Details</label>
                <textarea name="other_details" class="textarea "></textarea>

                <button class="button is-link" type="submit">Submit</button>
            </div>
        </div>

    </form>
@endsection

