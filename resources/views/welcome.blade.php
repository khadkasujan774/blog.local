@extends('layout')

@section('content')
    {{--<img src="/gradient_bg.jpg" alt="">--}}

    <div class="container">
        <div class="topnav">
            <ul>
                <li><a class="active" href="/">Home</a></li>
                <li><a href="/news">News</a></li>
                <li><a href="apps">APPS</a></li>
                <li><a href="/mobiles">MOBILE</a></li>
                <li><a href="/startup">STARTUP</a></li>
                <li><a href="/extra">Extra</a></li>
                {{-- <li class="submenu">
                     <a href="#contact">DEV & DESIGN</a>
                     <ul>
                         <li><a href="#">test1</a></li>
                         <li>
                             <a href="#">test2</a>
                             <ul>
                                 <li><a href="#">test test 1</a></li>
                                 <li><a href="#">test test 2</a></li>
                             </ul>
                         </li>
                         <li><a href="#">test3</a></li>
                     </ul>
                 </li>--}}
            </ul>
            @if(auth()->user())
                <a href="/logout">Logout</a>
            @else
                <a href="/login">Login</a>
            @endif
        </div>
    </div>

    {{--<home-page></home-page>--}}
   {{-- <form action="" method="post" enctype="multipart/form-data">
        @csrf
        <label>Select image to upload:</label>
        <input type="file" name="file" id="file">
        <input type="submit" value="Upload" name="submit">
    </form>--}}
   <div class="search">
       <form action="/search" method="post">
           @csrf
           <div class="field has-addons">
               <div class="control">
                   <input class="input" type="text" placeholder="Search..." name="username">
               </div>
               <div class="control">
                   <button>search</button>
               </div>
           </div>
       </form>
   </div>
    <div>
        <h1>Total user is :{{$users->total()}}</h1>
        <h1>User per page :{{$users->count()}}</h1>
        <h1>Current page is :{{$users->currentPage()}}</h1>
    </div>
    <table class="table is-bordered">
        <thead>
            <tr>
                <th>Id</th>
                <th>Username</th>
                <th>Email</th>
                <th>email_verified_at</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->username}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->email_verified_at}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
   <div class="section">
       {{--@foreach($users as $user)

               <p>{{$user->id}}. {{$user->username}}</p>

           @endforeach--}}
   </div>
    <div class="pagination">
        <p>{{ $users->links() }}</p>
    </div>
    @endsection
