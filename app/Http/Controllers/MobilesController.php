<?php

namespace App\Http\Controllers;

use App\Apps;
use App\Mobile;
use Illuminate\Http\Request;
use DB;

class MobilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('mobiles.mobilepage');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'image' => ['required'],
            'brand' => ['required', 'max:50'],
            'model_name' => ['required'],
            'price' => ['required', 'numeric'],
            'display' => ['required'],
            'color' => ['required'],
            'os' => ['required'],
            'ram' => ['required'],
            'storage' => ['required'],
            'rear_camera' => ['required'],
            'front_camera' => ['required'],
            'battery_capacity' => ['required'],
        ]);
        $image = request()->file('image');

        $filename = uniqid('ff') . '.' . $image->extension();
        $image->storeAs('public', $filename);

        $fileModel = new Mobile();
        $fileModel->image = $filename;
        $fileModel->brand = request('brand');
        $fileModel->model_name = request('model_name');
        $fileModel->price = request('price');
        $fileModel->display = request('display');
        $fileModel->color = request('color');
        $fileModel->os = request('os');
        $fileModel->ram = request('ram');
        $fileModel->storage = request('storage');
        $fileModel->rear_camera = request('rear_camera');
        $fileModel->front_camera = request('front_camera');
        $fileModel->battery_capacity = request('battery_capacity');
        $fileModel->other_details = request('other_details');
        $fileModel->save();

//        Mobile::create(request(['brand', 'model_name', 'price', 'description']));
        return redirect('mobiles');


        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mobile $mobile
     * @return \Illuminate\Http\Response
     */
    public function show(Mobile $mobile)
    {
        //
        $mobiles = Mobile::orderBy('price')->paginate(1);
        return view('mobiles.mobilepage', ['mm' => $mobiles]);
    }

    public function getDetails(Mobile $mobile)
    {
        return view('mobiles.mobile_details', compact('mobile'));
        /*return view('mobiles.mobile_details');*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mobile $mobile
     * @return \Illuminate\Http\Response
     */
    public function edit(Mobile $mobile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Mobile $mobile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mobile $mobile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mobile $mobile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mobile $mobile)
    {
        //
    }

    public function add()
    {
        return view('mobiles.addmobile');
    }
}
