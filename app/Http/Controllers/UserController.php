<?php

namespace App\Http\Controllers;

use App\Apps;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function getUsers()
    {
        $users = User::paginate(15);
        return view('welcome', ['users' => $users]);
    }
    public function searchUser()
    {

        /*$users = User::where('username', 'like', '%'.request('username').'%')->paginate(5);
        return view('welcome')->with('users', $users);*/
        $users = User::where('username', 'like', '%'.request('username').'%')->paginate(5);
        return view('welcome', compact('users'));
    }
}
