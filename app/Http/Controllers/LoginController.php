<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index()
    {
        if (auth()->user()) {
            return redirect('/news');
        }
        return view('login');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login()
    {
//       $file = request()->file('file');
//      $file->move(public_path(), $file->getClientOriginalName());
//      dd($file)
        request()->validate([
            'username' => ['required', 'min:3'],
            'password' => ['required', 'min:3']
        ]);
        if (auth()->attempt(request(['username', 'password']))) {
            return redirect('/news');
        }
        session()->flash('auth', 'User not found');
        return redirect('/login');
    }

    /**
     * logout function
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        if (auth()->user()) {
            auth()->logout();
            session()->flash('auth', 'User is logged out');
        }
        return redirect('/login');
    }
}
