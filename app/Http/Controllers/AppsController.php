<?php

namespace App\Http\Controllers;

use App\Apps;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class AppsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apps = Apps::all();
        return view('apps.apppage', ['tt' => $apps]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('apps.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function store(Request $request)
    {
        /*if ($request->hasFile('image')) {
            $request->file('image');
            //return $request->image->extension();
            return $request->image->storeAs('public', 'aa.jpg');
            //return Storage::putFile('public', $request->file('image'));
        } else {
            return 'No file selected';
        }*/
        if ($request->hasFile('image')) {
            foreach ($request->image as $image) {
                $filename = uniqid('ff') . '.' . $image->extension();
                $filesize = $image->getClientsize();
                $image->storeAs('public', $filename);
                $fileModel = new Apps;
                $fileModel->name = $filename;
                $fileModel->size = $filesize;
                $fileModel->save();
                //print_r($filename."<br>");

            }
            return 'yes';
        }
        return $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Apps $apps
     * @return string
     */
    public function show(Apps $apps)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Apps $apps
     * @return \Illuminate\Http\Response
     */
    public function edit(Apps $apps)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Apps $apps
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Apps $apps)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Apps $apps
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Apps::find($id);
        unlink(public_path('storage/' . $data['name']));
        $data->delete();
        return redirect('/apps');
    }

    public function download($id)
    {
        $aa = Apps::find($id);

        $file_path = public_path('storage/' . $aa['name']);
//        dd($file_path, $aa);
        return response()->download($file_path);
    }
}
