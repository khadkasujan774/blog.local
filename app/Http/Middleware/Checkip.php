<?php

namespace App\Http\Middleware;

use Closure;

class Checkip
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current_ip = $_SERVER['REMOTE_ADDR'];

        if (!in_array($current_ip, config('custom.allowed_ip'))) {
            //abort(404);
        }
        return $next($request);
    }
}
