<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobile extends Model
{
    //
    protected $fillable = [
      'brand',
      'model_name',
      'price',
      'description'
    ];
}
