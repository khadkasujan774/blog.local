<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
   //
    protected $guarded = [];
    private $comment;

    public function news()
    {
        return $this->belongsTo(News::class);
    }
}
